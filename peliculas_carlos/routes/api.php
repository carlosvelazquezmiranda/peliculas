<?php

use App\Http\Controllers\Gestion_de_peliculasController;
use App\Http\Controllers\Gestion_de_directoresController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


//Route::get('peliculas_carlos.test/api/Gestion_de_peliculas',[Gestion_de_peliculasController::class,'index']);

Route::apiResource('Gestion_de_peliculas', Gestion_de_peliculasController::class);
Route::apiResource('Gestion_de_directores', Gestion_de_directoresController::class);
Route::delete('Gestion_de_peliculas/{id}', [Gestion_de_peliculasController::class, 'destroy'])->name('Gestion_de_peliculas.destroy');


