<?php

namespace App\Http\Controllers;

use App\Models\peliculas;
use Illuminate\Http\Request;

class Gestion_de_peliculasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
   
     public function index()
     {
        echo "mundito";
     }
 
     /**
      * Show the form for creating a new resource.
      */
     public function create()
     {
         echo "hola";
     }
 
     /**
      * Store a newly created resource in storage.
      */
     public function store(Request $request)
     {
 
 
         $se_repite =  count(peliculas::where('nombre_pelicula',  $request->nombre_pelicula)
         ->get());
 
        // dd($se_repite);
 
         if ($se_repite == 0) {
             peliculas::create(array(
                 'nombre_pelicula' => $request->nombre_pelicula,
                 'id_director' => $request->director,
                 'anio_lanzamiento' => $request->anio_lanzamiento,
                 'descripcion' => $request->descripcion
             ));
         }else{
             peliculas::where('id_director', $request->director)
             ->update(['anio_lanzamiento' =>  $request->anio_lanzamiento, 'descripcion' =>  $request->descripcion]);
         }
         
 
         $peliculas = peliculas::all();
         $directores = director::all();
        
        // $directores = peliculas::distinct()->get(['director']);
         $numero_directores = count($directores);
         //dd($numero_directores);
         $lista = [];
         for($i = 0; $i < $numero_directores; $i++){
             $num_peliculas = peliculas::where('id_director', $directores[$i]['id_director'])
             ->count();
 
             $lista[$i]['director'] = $directores[$i]['nombre_director'];
             $lista[$i]['No_peliculas'] = $num_peliculas;
         }
 
         return json_encode(['ok'=> $lista] );
     }
 
     /**
      * Display the specified resource.
      */
     public function show(string $id)
     {
        
         
 
 
 
     }
 
     /**
      * Show the form for editing the specified resource.
      */
     public function edit(string $id)
     {
         //
     }
 
     /**
      * Update the specified resource in storage.
      */
     public function update(Request $request, string $id)
     {
         //
     }
 
     /**
      * Remove the specified resource from storage.
      */
 
      public function destroy(int $id_peliculas)
     {
         //echo "eliminA". $id;
         peliculas::where('id_peliculas', $id_peliculas)
             ->delete();
     }
     
     /*
     public function destroy(peliculas $id_peliculas) {
        // $id_peliculas->delete();
   }
 */
}
