<?php

namespace App\Http\Controllers\Gestion_de_peliculas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class panelController extends Controller
{
    /**
     * Display a listing of the resource.
     */




    public function index()
    {
       
        return json_encode(['ok' =>'ok']);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return json_encode(['ok' =>'ok']);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return json_encode(['ok' =>'ok']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


   

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
